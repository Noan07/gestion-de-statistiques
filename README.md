# SAE2-04

# Liens utile

Script Anglais:
https://docs.google.com/document/d/1fxkne0itcujQr36Z-6jINY0P3caSRCbogTsEUhYhqBM/edit?usp=sharing

Rapport:
https://docs.google.com/document/d/1PEjMDtbbBWiwBR4SgACMYxAuAT-TXJwroU_p4dWh-Bg/edit?usp=sharing 

Diapo:
https://docs.google.com/presentation/d/1bRv7Zdit6q2em3CsBk6vtCsXe7jvFUi6Ee5EwZR8vhs/edit?usp=sharing

## Liens jeu de donnée

https://www.kaggle.com/datasets/muhmores/spotify-top-100-songs-of-20152019

https://www.kaggle.com/datasets/pieca111/music-artists-popularity

## Note Python:

liste commande interaction BDD:

Pour récupérer les données csv dans une data frame (pseudo BDD python) traité par pandas:
- df = pandas.DataFrame(pandas.read_csv(filename))

Pour intéragir avec postregreesql on utilise pyscopg2 qui est une biliothèque:
- connection = psycopg2.connect(host=db_host, port=5432, database=db_name, user=db_user, password=db_password)

Dans pandas: La méthode duplicated() retourne un booléen pour chaque ligne du DataFrame sur lequel la méthode est appelée indiquant si la ligne est un doublon ou non (autrement dit, si elle est égale à une ligne précédente). 

Dans pandas:
- Drop_duplicates() retourne un DataFrame équivalent sans les lignes en doublon.

Gérer les données manquantes:
- On peut les supprimer ou les remplacer:

Pour supprimer les lignes concernées, on peut utiliser la méthode dropna().

Pour remplacer les valeurs manquantes, on peut utiliser la méthode fillna().

## Détails du jeux de données de spotify
- title, -- Titre de la musique
- artist, -- Artiste
- genre, -- Genre de la musique
- release -- Année de réalisation de la musique
- added, -- La chanson du jour a été ajoutée à la liste de lecture Top Hits de Spotify
- bpm, -- Le tempo de la musique
- nrgy, -- À quel point la chanson est-elle énergique
- dnce, -- La facilité à danser sur la chanson
- dB, -- À quel point la chanson est-elle forte
- live, -- La probabilité que la chanson soit un enregistrement en direct
- val, -- À quel point l'ambiance de la chanson est positive
- dur, -- Durée de la musique
- acous, -- À quel point la chanson est acoustique
- spch, -- À quel point est axée sur la parole
- pop, -- Popularité de la chanson (pas un classement)
- top_year, -- L'année où la chanson a été un grand succès
- artist_type, -- Indique si l'artiste est en solo, en duo, en trio ou en groupe

### Données non pris

## Détails du jeu de données de Artiste
- mbid, -- Identifiant
- artist_mb, -- Nom de l'artiste
- country_mb, -- Pay de l'artiste
- tags_mb, -- Gendre de la musique
- listeners_lastfm, -- Les personnes qui écoute cette Artiste

### Données non pris
- artist_lastfm, -- doublons du nom d'artiste 
- country_lastfm, -- doublons du pays de l'artiste
- tags_lastfm, -- doublons du genre de la musique
- scrobbles_lastfm, -- ?
- ambiguous_artist, -- Vrai si plusieurs artistes partagent la même page last.fm
