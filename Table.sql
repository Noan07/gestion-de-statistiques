/*========================================================================

Script        : Table.sql
Author        : Dufour Louis & Noan Randon & Axel Cotin
Creation date : 07-05-2022

========================================================================*/

DROP TABLE IF EXISTS Plays;
DROP TABLE IF EXISTS Music;
DROP TABLE IF EXISTS Artist;
DROP TABLE IF EXISTS Country;
DROP TABLE IF EXISTS Genre;

CREATE TABLE IF NOT EXISTS Country
(
        ID INT PRIMARY KEY,
        Name_country VARCHAR(500) CONSTRAINT doublons_country UNIQUE NOT NULL
);

CREATE TABLE IF NOT EXISTS Artist
(
        ID INT PRIMARY KEY,
        IDCountry INT NOT NULL REFERENCES Country(ID),
        artist VARCHAR(150) CONSTRAINT doublons_artiste UNIQUE NOT NULL,
        artist_type CHAR(30) NOT NULL,
        CHECK(artist_type IN ('Band/Group','Trio','Duo','Solo'))
);

CREATE TABLE IF NOT EXISTS Genre
(
        ID INT PRIMARY KEY,
        nameGenre VARCHAR(150) CONSTRAINT doublons_genre UNIQUE NOT NULL 
);

CREATE TABLE IF NOT EXISTS Music
(
        ID INT PRIMARY KEY, 
        artist_musique INT REFERENCES Artist(ID),
        IDGenre INT NOT NULL REFERENCES Genre(ID),
        title VARCHAR(150) NOT NULL,
        bpm NUMERIC NOT NULL CHECK(bpm >= 0),
        dnce NUMERIC NOT NULL CHECK(dnce >= 0),
        live NUMERIC NOT NULL CHECK(live >= 0),
        spch NUMERIC NOT NULL CHECK(spch >= 0),
        pop NUMERIC NOT NULL CHECK(pop >= 0)
);

CREATE TABLE IF NOT EXISTS Plays
(
        IDArtist INT PRIMARY KEY REFERENCES Artist,
        IDGenre INT REFERENCES Genre
);