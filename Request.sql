/*========================================================================

Script        : Request.sql
Author        : Dufour Louis, Noan Randon
Creation date : 07-05-2022

========================================================================*/

-- 1) Les 10 genres les plus joués par les artistes
DROP VIEW IF EXISTS GenrePlusJoue;

Create VIEW GenrePlusJoue As
Select g.nameGenre,count(a.*) nbGenre
From Genre g, Plays p, Artist a
Where p.IDGenre=g.Id And a.ID=p.IDArtist
Group by g.nameGenre
Order by nbGenre DESC
Limit 10;

-- 2) Les 10 genres les plus écoutés à partir du top 1000 des musiques Spotify
DROP VIEW IF EXISTS GenrePlusEcoute;

Create VIEW GenrePlusEcoute As
Select g.nameGenre,count(m.*) 
From Music m, Genre g 
Where g.ID=m.IDGenre
GROUP BY g.nameGenre
ORDER BY count(m.*) DESC
FETCH FIRST 10 ROWS ONLY;


-- 3) Les pays avec le plus de musiques faisant partie du top 1000 des musiques Spotify
DROP VIEW IF EXISTS Top10MusiquesParPays;

Create VIEW Top10MusiquesParPays As
Select c.name_country, count(m.*)nbMusic
From Music m, Artist a, Country c
Where m.artist_musique=a.ID And c.ID=a.IDCountry
Group by c.name_country
Order by nbMusic DESC
LIMIT 10;

-- 4) Les 10 sons les plus populaires en fonction de leur taux de parole
DROP VIEW IF EXISTS ParoleEtGenre;

Create VIEW ParoleEtGenre As
Select title, pop, avg(spch) avgSpch
From Music 
GROUP BY title, pop
ORDER BY pop  DESC ,avgSpch DESC
LIMIT 10;


-- 5 ) Popularité d'une musique en fonction de son BPM
DROP VIEW IF EXISTS PopMusiqueEtBPM;

Create VIEW PopMusiqueEtBPM As
Select title, pop, avg(bpm) avgBpm
From Music 
GROUP BY title, pop
ORDER BY pop  DESC ,avgBpm DESC
LIMIT 10;

-- 6) Les 10 genres avec le plus de danceability
DROP VIEW IF EXISTS GenreAvecLePlusDeDnce;

Create VIEW GenreAvecLePlusDeDnce As 
Select g.nameGenre, avg(m.dnce) avgDnce
From Music m, Genre g
Where g.ID=m.IDGenre
GROUP BY g.nameGenre
ORDER BY (avgDnce) DESC
LIMIT 10;

-- 7) Pourcentage du nombre de musiques par type d’artiste
DROP VIEW IF EXISTS ArtistTypePop;

Create VIEW ArtistTypePop As
Select a.artist_type, count(m.*)nbM 
From Music m, Artist a
Where m.artist_musique=a.id
Group by a.artist_type 
Order by nbM DESC;

-- 8) Les 10 artistes qui enregistrent le plus souvent en live
DROP VIEW IF EXISTS ArtistMusiqueLive;

Create VIEW ArtistMusiqueLive As
Select a.artist,m.live
From Artist a, Music m Where a.Id=m.artist_musique
Order by live DESC
LIMIT 10;

-- 9) Les 10 artistes les plus populaires

DROP VIEW IF EXISTS ArtistPop;

Create VIEW ArtistPop As
Select artist ,avg(m.pop) avgPop
From Music m, Artist a
Where m.artist_musique=a.id
Group by artist Order by avgPop Desc
LIMIT 10;

-- 10) Les 10 des genres les plus populaires
DROP VIEW IF EXISTS GenrePop;

Create VIEW GenrePop As
Select g.nameGenre,avg(pop) avgPop
From Genre g, Music m
Where m.IDGenre=g.Id 
Group by g.nameGenre
Order by avgPop DESC
Limit 10;
