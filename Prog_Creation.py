# ========================================================================
#
# Script        : Prog_Visual.py
# Author        : Dufour Louis
# Creation date : 11-04-2022
#
# ========================================================================

import pandas as pand # pip3 install pandas
import psycopg2 # pip3 install types-psycopg2
from getpass import getpass

################# Debut Tips #################

# Pour lancer le script il faut être dans le réseau de l'iut
# Il faut aussi se trouver dans le répertoire où se trouvent les fichiers sinon le script ne trouve pas les fichiers sql

################# Fin Tips #################

def create_tables(connection, filename: str):
    cur = connection.cursor()
    with open(filename) as f:
        cur.execute(f.read())
    connection.commit()
    cur.close()
    
def desctruct_view(connection, filename: str):
    cur = connection.cursor()
    with open(filename) as f:
        cur.execute(f.read())
    connection.commit()
    cur.close()

def create_view(connection, filename: str):
    cur = connection.cursor()
    with open(filename) as f:
        cur.execute(f.read())
    connection.commit()
    cur.close()

def insert_Plays(connection):
    cur = connection.cursor()
    df = pand.DataFrame(pand.read_sql("SELECT ID FROM Artist;", con=connection))
    for row1 in df.itertuples():
        cur.execute("INSERT INTO Plays VALUES (%s);",(str(row1.id),))


    df2 = pand.DataFrame(pand.read_sql("SELECT IDGenre FROM Music;", con=connection))
    for row2 in df2.itertuples():            
        cur.execute("SELECT artist_musique FROM Music WHERE IDGenre = %s;", (str(row2.idgenre),))
        for rArtist in cur.fetchall():
            cur.execute("UPDATE Plays SET IDGenre = %s WHERE IDArtist = %s;",
                        (str(row2.idgenre),
                        rArtist)
                        )         
         
    connection.commit()
    cur.close()
    
def insert_Music(connection, filename: str):
    cur = connection.cursor()
    df = pand.DataFrame(pand.read_csv(filename)).dropna()

    id_table=0
    for row in df.itertuples():
        id_table+=1

        cur.execute("SELECT ID FROM Genre WHERE nameGenre = %s;",(str(row.top_genre),))
        rGenre = cur.fetchone()[0]
        if rGenre is None:
            continue
        else:
            rGenre

        cur.execute("SELECT ID FROM Artist WHERE artist = %s;",(str(row.artist),))
        rArtist = cur.fetchone()
        if rArtist is None:
            continue
        else:
            rArtist
 

        #explication de pourquoi c'est que des %s : https://www.psycopg.org/docs/usage.html#passing-parameters-to-sql-queries
        cur.execute("INSERT INTO Music VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s);",
                    (id_table,
                    rArtist,
                    rGenre,
                    str(row.title),
                    row.bpm,
                    row.dnce,
                    row.live,
                    row.spch,
                    row.pop)
                    )

    connection.commit()
    cur.close()

def insert_Artist(connection, music_filename: str, artist_filename: str):
    cur = connection.cursor()
    music_df = pand.DataFrame(pand.read_csv(music_filename)).dropna()
    artist_df = pand.DataFrame(pand.read_csv(artist_filename)).dropna()

    id_table=0
    for row1 in artist_df.itertuples():
        for row2 in music_df.itertuples():
            if str(row1.artist_lastfm) == str(row2.artist):
                id_table+=1
                cur.execute("""INSERT INTO Artist (ID, idcountry, artist, artist_type)
                VALUES (%s, (
                    SELECT id FROM Country WHERE Name_country = %s
                ), %s, %s)
                ON CONFLICT ON CONSTRAINT doublons_artiste DO NOTHING;""",
                            (id_table,
                            row1.country_mb,
                            str(row2.artist),
                            str(row2.artist_type))
                            )
                break

    connection.commit()
    cur.close()
    
def insert_Genre(connection, filename: str):
    cur = connection.cursor()
    df = pand.DataFrame(pand.read_csv(filename)).dropna()

    id_table=0
    for row in df.itertuples():
        id_table+=1
        cur.execute("INSERT INTO Genre VALUES (%s, %s) ON CONFLICT ON CONSTRAINT doublons_genre DO NOTHING;",
                    (id_table,
                    str(row.top_genre))
                    )

    connection.commit()
    cur.close()
  
def insert_Country(connection, filename: str):
    cur = connection.cursor()
    df = pand.DataFrame(pand.read_csv(filename)).dropna()

    id_table=0
    for row in df.itertuples():
        for country in row.country_lastfm.split('; '): 
            id_table+=1
            cur.execute("INSERT INTO Country VALUES (%s, %s) ON CONFLICT ON CONSTRAINT doublons_country DO NOTHING;",
                        (id_table,
                        str(country))
                        )

    connection.commit()
    cur.close()
  

# lors de l'execution le main s'exectute et en plus si on l'import autre part cela n'executera pas le code
if __name__ == '__main__':
    db_host = input('Nom d\'hôte : ')
    if not db_host:
        db_host = 'berlin'
    db_name = input('Nom de la base de données : ')
    if not db_name:
        db_name = 'dblodufour1'
    db_user = input('Utilisateur : ')
    if not db_user:
        db_user = 'lodufour1'
    db_password = getpass('Mot de passe : ')

    connection = psycopg2.connect(host=db_host, port=5432, database=db_name, user=db_user, password=db_password)
        
    desctruct_view(connection, 'Desctruct_View.sql')
    create_tables(connection, 'Table.sql')
    create_view(connection, 'Request.sql')    
    insert_Country(connection, 'artists.csv')
    insert_Artist(connection, 'Spotify_2010_2019.csv', 'artists.csv')
    insert_Genre(connection, 'Spotify_2010_2019.csv')
    insert_Music(connection, 'Spotify_2010_2019.csv')
    insert_Plays(connection)
    connection.close()
