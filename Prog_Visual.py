# ========================================================================
#
# Script        : Prog_Visual.py
# Author        : Dufour Louis
# Creation date : 11-04-2022
#
# ========================================================================

from cProfile import label
from sys import api_version
import psycopg2 # pip3 install types-psycopg2
import matplotlib.pyplot as plt
import pandas as pand # pip3 install pandas
from getpass import getpass

################# Debut Tips #################

# Pour lancer le script il faut être dans le réseau de l'iut
# Il faut aussi se trouver dans le répertoire où se trouvent les fichiers sinon le script ne trouve pas les fichiers sql

################# Fin Tips #################
  
def Top10GenrePlay(connection):
    """Requête N°1"""
    cur = connection.cursor()
    cur.execute("SELECT nbGenre FROM GenrePlusJoue")
    NbGenre= cur.fetchall()
   
    cur.execute("SELECT nameGenre FROM GenrePlusJoue")
    NameGenre = cur.fetchall()


    ListeNameGenre=[]
    ListeNbGenre=[]
    for Genre in NameGenre:
        ListeNameGenre.append(Genre[0])

    for i in NbGenre:
        ListeNbGenre.append(i[0])

         
    labels = ListeNameGenre
    sizes = ListeNbGenre
    explode = [0.1 if i == 0 else 0 for i in range(len(ListeNameGenre))] # only "explode" the 2nd slice (i.e. 'Hogs')

    fig1, ax1 = plt.subplots()
    ax1.pie(sizes, labels=labels, explode=explode, shadow=True, autopct='%1.1f%%', startangle=90)
    ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.

def Top10GenreLisen(connection):
    """Requête N°2"""
    cur = connection.cursor()
    cur.execute("SELECT count FROM GenrePlusEcoute")
    NbGenre= cur.fetchall()
   
    cur.execute("SELECT nameGenre FROM GenrePlusEcoute")
    NameGenre = cur.fetchall()


    ListeNameGenre=[]
    ListeNbGenre=[]
    for Genre in NameGenre:
        ListeNameGenre.append(Genre[0])

    for i in NbGenre:
        ListeNbGenre.append(i[0])

         
    labels = ListeNameGenre
    sizes = ListeNbGenre
    explode = [0.1 if i == 0 else 0 for i in range(len(ListeNameGenre))] # only "explode" the 2nd slice (i.e. 'Hogs')

    fig1, ax1 = plt.subplots()
    ax1.pie(sizes, labels=labels, explode=explode, shadow=True, autopct='%1.1f%%', startangle=90)
    ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.
    
def Top10MusiquesParPays(connection):
    """Requête N°3"""
    df = pand.read_sql("SELECT name_country, nbmusic FROM Top10MusiquesParPays", con=connection)
    fig = df.plot.bar(x='name_country', y='nbmusic', legend=False, rot=0)
    fig.set_xlabel('Country')
    fig.set_ylabel('Music count')
    fig.bar_label(fig.containers[0]) # Show values


def MusicEtSpeach(connection):
    """Requête N°4"""
    df = pand.read_sql("SELECT title, avgspch FROM ParoleEtGenre", con=connection)
    fig = df.plot.barh(x='title', y='avgspch', legend=False, rot=0)
    fig.invert_yaxis()
    fig.set_xlabel('Speech rate')
    fig.set_ylabel('')
    fig.bar_label(fig.containers[0]) # Show values

def MusicEtBpm(connection):
    """Requête N°5"""
    df = pand.read_sql("SELECT title, avgbpm FROM PopMusiqueEtBPM", con=connection)
    fig = df.plot.barh(x='title', y='avgbpm', legend=False, rot=0)
    fig.invert_yaxis()
    fig.set_xlabel('BPM')
    fig.set_ylabel('')
    fig.bar_label(fig.containers[0]) # Show values

def Top10DesGenreWithDance(connection):
    """Requête N°6"""
    df = pand.read_sql("SELECT namegenre, avgdnce FROM GenreAvecLePlusDeDnce", con=connection)
    fig = df.plot.barh(x='namegenre', y='avgdnce', legend=False, rot=0)
    fig.invert_yaxis()
    fig.set_xlabel('Dance ability')
    fig.set_ylabel('')
    fig.bar_label(fig.containers[0]) # Show values

def ArtistTypePop(connection):
    """Requête N°7"""
    cur = connection.cursor()
    cur.execute("SELECT nbm FROM ArtistTypePop")
    Nb= cur.fetchall()
   
    cur.execute("SELECT artist_type FROM ArtistTypePop")
    NameType = cur.fetchall()

    ListeNameType=[]
    ListeNb=[]
    for Artist_type in NameType:
        ListeNameType.append(Artist_type[0])

    for i in Nb:
        ListeNb.append(i[0])
         
    labels = ListeNameType
    sizes = ListeNb
    explode = [0.1 if i == 0 else 0 for i in range(len(ListeNameType))] # only "explode" the 2nd slice (i.e. 'Hogs')

    fig1, ax1 = plt.subplots()
    ax1.pie(sizes, labels=labels, explode=explode, shadow=True, autopct='%1.1f%%', startangle=90)
    ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.

def Top10ArtistLive(connection):
    """Requête N°8"""
    df = pand.read_sql("SELECT artist, live FROM ArtistMusiqueLive", con=connection)
    fig = df.plot.bar(x='artist', y='live', legend=False, rot=0)
    fig.set_xlabel('Artist')
    fig.set_ylabel('Live (%)')
    fig.bar_label(fig.containers[0]) # Show values

def Top10ArtistPop(connection):
    """Requête N°9"""
    df = pand.read_sql("SELECT artist, avgpop FROM ArtistPop", con=connection)
    fig = df.plot.bar(x='artist', y='avgpop', legend=False, rot=0)
    fig.set_xlabel('Artist')
    fig.set_ylabel('Popularity (%)')
    fig.bar_label(fig.containers[0]) # Show values

def Top10GenrePop(connection):
    """Requête N°11"""
    df = pand.read_sql("SELECT namegenre, avgpop FROM GenrePop", con=connection)
    fig = df.plot.barh(x='namegenre', y='avgpop', legend=False, rot=0)
    fig.invert_yaxis()
    fig.set_xlabel('Popularity')
    fig.set_ylabel('')
    fig.bar_label(fig.containers[0]) # Show values


#lors de l'execution le main s'exectute et en plus si on l'import autre part cela n'executera pas le code
if __name__ == '__main__':
    db_host = input('Nom d\'hôte : ')
    if not db_host:
        db_host = 'berlin'
    db_name = input('Nom de la base de données : ')
    if not db_name:
        db_name = 'dblodufour1'
    db_user = input('Utilisateur : ')
    if not db_user:
        db_user = 'lodufour1'
    db_password = getpass('Mot de passe : ')

    connection = psycopg2.connect(host=db_host, port=5432, database=db_name, user=db_user, password=db_password)
    
    Top10GenrePlay(connection)         
    Top10GenreLisen(connection)              
    Top10MusiquesParPays(connection)    
    MusicEtSpeach(connection)          
    MusicEtBpm(connection)            
    Top10DesGenreWithDance(connection)  
    ArtistTypePop(connection)           
    Top10ArtistLive(connection)        
    Top10ArtistPop(connection)
    Top10GenrePop(connection)           
    
    connection.close()
    plt.show()